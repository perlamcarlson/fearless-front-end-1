import { NavLink } from "react-router-dom";


function Nav() {
  <nav>
    <NavLink className="nav-link" aria-current="page" to="/locations/new">New location</NavLink>
    <NavLink className="nav-link" aria-current="page" to="/conferences/new">New conference</NavLink>
    <NavLink className="nav-link" aria-current="page" to="/attendees/new">New attendees</NavLink>
    <NavLink className="nav-link" aria-current="page" to="/attendees">Attendees list</NavLink>
    <NavLink className="nav-link" aria-current="page" to="/presentations/new"> New presentation</NavLink>
  </nav>
  
    ;
  }
  export default Nav;