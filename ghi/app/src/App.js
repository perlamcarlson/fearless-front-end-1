import React from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import ConferenceForm from './ConferenceForm';
import AttendeeConferenceForm from './AttendConferenceForm';
import LocationForm from './LocationForm';
import AttendeesList from './AttendeesList';
import PresentationForm from './PresentationForm';



function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
    <Nav />
    <div className="container">
      <Routes>
        <Route path="conferences/new" element={< ConferenceForm />}/>
          <Route path="attendees/new" element={< AttendeeConferenceForm />}/>
              <Route path="locations/new" element={< LocationForm />}/>
                <Route path="attendees" element={< AttendeesList attendees={props.attendees} />}/> 
                <Route path="presentation/new" element={< PresentationForm />}/>
          <Route index element={<MainPage />} />
      </Routes>
      </div>
    </BrowserRouter>
  );
      
  
}
export default App;